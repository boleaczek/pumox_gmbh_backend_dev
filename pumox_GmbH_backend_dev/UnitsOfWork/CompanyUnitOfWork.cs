using System.Threading.Tasks;
using pumox_GmbH_backend_dev.Context;
using pumox_GmbH_backend_dev.Models;
using pumox_GmbH_backend_dev.Repositories;

namespace pumox_GmbH_backend_dev.UnitsOfWork
{
    public class CompanyUnitOfWork : ICompanyUnitOfWork
    {
        public IRepository<Company> CompanyRepository { get; set; }
        public IRepository<Employe> EmployeRepository { get; set; }
        
        CompanyContext _context;

        public CompanyUnitOfWork(CompanyContext context)
        {
            CompanyRepository = new Repository<Company>(context);
            EmployeRepository = new Repository<Employe>(context);
            _context = context;
        }

        public async Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}