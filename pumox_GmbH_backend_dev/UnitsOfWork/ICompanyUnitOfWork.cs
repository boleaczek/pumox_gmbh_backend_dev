using System.Threading.Tasks;
using pumox_GmbH_backend_dev.Models;
using pumox_GmbH_backend_dev.Repositories;

namespace pumox_GmbH_backend_dev.UnitsOfWork
{
    public interface ICompanyUnitOfWork
    {
        IRepository<Company> CompanyRepository { get; set; }
        IRepository<Employe> EmployeRepository { get; set; }
        Task<int> SaveAsync();
    }
}