using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace pumox_GmbH_backend_dev.Models
{
    public class Company
    {
        [Required]
        public long Id { get; set; }
        [Required]
        public string Name {get; set;}
        [Required]
        public int? EstablishmentYear { get; set; }
        public ICollection<Employe> Employes { get; set; }
    }
}