using System;
using System.Collections.Generic;

namespace pumox_GmbH_backend_dev.Models
{
    public class CompanyQuerry
    {
        public string Keyword { get; set; }
        public DateTime? EmployeDateOfBirthFrom { get; set; }
        public DateTime? EmployeDateOfBirthTo { get; set; }
        public IEnumerable<Title> EmployeJobTitles {get; set; }
    }
}