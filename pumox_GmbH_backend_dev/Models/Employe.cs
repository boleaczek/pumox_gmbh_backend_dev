using System;
using System.ComponentModel.DataAnnotations;

namespace pumox_GmbH_backend_dev.Models
{
    public enum Title{
        Administrator,
        Developer,
        Architect,
        Manager
    }
    public class Employe
    {
        
        [Required]
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
        [Required]
        public Title JobTitle { get; set; }
    }
}