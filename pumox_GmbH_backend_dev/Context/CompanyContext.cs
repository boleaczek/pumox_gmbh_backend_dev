using Microsoft.EntityFrameworkCore;
using pumox_GmbH_backend_dev.Models;

namespace pumox_GmbH_backend_dev.Context
{
    public class CompanyContext : DbContext
    {   
        public CompanyContext(DbContextOptions<CompanyContext> options) : base(options)
        {
            
        }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Employe> Employes { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employe>().HasOne<Company>().WithMany(e => e.Employes).OnDelete(DeleteBehavior.Cascade);
        }

    }
}