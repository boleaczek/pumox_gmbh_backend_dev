using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using pumox_GmbH_backend_dev.Models;
using pumox_GmbH_backend_dev.UnitsOfWork;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace pumox_GmbH_backend_dev.Controllers
{
    [Authorize]
    [ApiController]
    [Route("/[controller]")]
    public class CompanyController : ControllerBase
    {
        ICompanyUnitOfWork _unitOfWork;

        public CompanyController(ICompanyUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        
        [HttpPost("create")]
        public async Task<ActionResult<string>> Post([FromBody] Company company)
        {
            _unitOfWork.CompanyRepository.Insert(company);
            
            if(!IsCompanyModelValid(company))
            {
                return BadRequest("Company name and establishment year cannot be null.");
            }

            await _unitOfWork.SaveAsync();
            return Ok($"{{Id:{company.Id}}}");       
        }
        
        [HttpPost("search")]
        public async Task<ActionResult<string>> Post([FromBody] CompanyQuerry querry)
        {
            if(querry.EmployeDateOfBirthFrom == null && querry.EmployeDateOfBirthTo != null)
            {
                querry.EmployeDateOfBirthFrom = DateTime.MinValue;
            }
            else if(querry.EmployeDateOfBirthTo == null && querry.EmployeDateOfBirthFrom != null)
            {
                querry.EmployeDateOfBirthTo = DateTime.MaxValue;
            }

            ICollection<Company> companies = await _unitOfWork.CompanyRepository
                .SearchFor(c => c.Employes.Any(
                            e => e.Name.Contains(querry.Keyword)
                            || e.LastName.Contains(querry.Keyword)
                            || (e.DateOfBirth >= querry.EmployeDateOfBirthFrom && e.DateOfBirth <= querry.EmployeDateOfBirthTo)
                            || (querry.EmployeJobTitles != null && querry.EmployeJobTitles.Contains(e.JobTitle))) 
                            || c.Name.Contains(querry.Keyword))
                .Include(c => c.Employes)
                .ToListAsync();
                
            return Ok(companies);
        }
        
        [HttpPut("companyupdate/{id}")]
        public async Task<ActionResult<string>> Put([FromRoute] long id, [FromBody] Company company)
        {
            if(!IsCompanyModelValid(company))
            {
                return BadRequest("Company name or establishment year cannot be null.");
            }

            company.Id = id;
            _unitOfWork.CompanyRepository.Update(company);
            await _unitOfWork.SaveAsync();

            return Ok("Company updated");
        }
        
        [HttpDelete("delete/{id}")]
        public async Task<ActionResult<string>> Delete([FromRoute] long id)
        {
            var company = await _unitOfWork.CompanyRepository.GetById(id);
            
            if(company == null)
            {
                return BadRequest("No company with specified id.");
            }

            _unitOfWork.CompanyRepository.Delete(company);
            await _unitOfWork.SaveAsync();

            return Ok("Company deleted");
        }

        #region helpers

        [NonAction]
        bool IsCompanyModelValid(Company company)
        {
            if(company.Name == null || company.EstablishmentYear == null)
            {
                return false;
            }

            return true;
        }

        #endregion
    }

}