using System.Security.Claims;
using System.Threading.Tasks;
using idunno.Authentication.Basic;

namespace pumox_GmbH_backend_dev.Security
{
    public class BasicAuthAuthenticationEvents : BasicAuthenticationEvents
    {
        public override Task ValidateCredentials(ValidateCredentialsContext context)
        {
            if(context.Password == "TOODXA" && context.Username == "UID")
                            {
                                var claims = new[]
                                {
                                    new Claim(
                                        ClaimTypes.NameIdentifier, 
                                        context.Username, 
                                        ClaimValueTypes.String, 
                                        context.Options.ClaimsIssuer),

                                };

                                context.Principal = new ClaimsPrincipal(
                                    new ClaimsIdentity(claims, context.Scheme.Name));
                                context.Success();
                            }
                            return Task.CompletedTask;
        }

    }
}