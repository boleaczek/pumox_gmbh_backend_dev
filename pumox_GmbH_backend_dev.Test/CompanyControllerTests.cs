using System;
using System.Threading.Tasks;
using Xunit;
using pumox_GmbH_backend_dev.Controllers;
using pumox_GmbH_backend_dev.Repositories;
using pumox_GmbH_backend_dev.UnitsOfWork;
using pumox_GmbH_backend_dev.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using System.Collections.Generic;
using pumox_GmbH_backend_dev.Test.Mocking;

namespace pumox_GmbH_backend_dev.Test
{
    public class CompanyControllerTests
    {
        IMockFactory _mockFactory;

        public CompanyControllerTests()
        {
            _mockFactory = new MockFactory();
        }

        [Fact]
        public void CompanyController_Create_recieves_valid_object_returns_good_request()
        {
            var unitOfWork = _mockFactory.CreateSimpleMockCompanyUnitOfWork();
            var companyController = new CompanyController(unitOfWork);

            var response = Task.Run(async () => {
                return await companyController.Post(new Company()
                {
                    Name = "test",
                    EstablishmentYear = 2018
                });
            }).GetAwaiter().GetResult();
            
            Assert.NotNull(response.Result as OkObjectResult);
        }

        [Fact]
        public void CompanyController_Create_recieves_invalid_object_returns_bad_request()
        {
            var unitOfWork = _mockFactory.CreateSimpleMockCompanyUnitOfWork();
            var companyController = new CompanyController(unitOfWork);
            
            var response = Task.Run(async() => {
                return await companyController.Post(new Company(){
                    Name = null,
                    EstablishmentYear = 2018
                });
            }).GetAwaiter().GetResult();

            Assert.NotNull(response.Result as BadRequestResult);
        }

        [Fact]
        public void CompanyController_Search_recieves_valid_querry_returns_good_request()
        {
            var unitOfWork = _mockFactory.CreateSimpleMockCompanyUnitOfWork();
            var companyController = new CompanyController(unitOfWork);

            var querry = new CompanyQuerry()
            {
                Keyword = "c"
            };

            var response = Task.Run(async() => {
                return await companyController.Post(querry);
            }).GetAwaiter().GetResult();
            
            Assert.NotNull(response.Result as OkObjectResult);
        }

        [Fact]
        public void ComapnyController_Update_recieves_valid_id_return_good_request()
        {
            var unitOfWork = _mockFactory.CreateSimpleMockCompanyUnitOfWork();
            var companyController = new CompanyController(unitOfWork);
            var response = Task.Run(async() => {
                return await companyController.Put(1, new Company()
                    {
                        Name="test",
                        EstablishmentYear=2018
                    });
            }).GetAwaiter().GetResult();

            Assert.NotNull(response.Result as OkObjectResult);
        }

        [Fact]
        public void CompanyController_Delete_recieves_valid_id_returns_good_requese()
        {
            var unitOfWork = _mockFactory.CreateMockCompanyUnitOfWorkHasCompany();
            var companyController = new CompanyController(unitOfWork);

            var response = Task.Run(async() => {
                return await companyController.Delete(1);
            }).GetAwaiter().GetResult();

            Assert.NotNull(response.Result as OkObjectResult);
        }
    }
}
