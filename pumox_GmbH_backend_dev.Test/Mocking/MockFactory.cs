using pumox_GmbH_backend_dev.UnitsOfWork;
using Moq;
using pumox_GmbH_backend_dev.Repositories;
using pumox_GmbH_backend_dev.Models;
using System;

namespace pumox_GmbH_backend_dev.Test.Mocking
{
    public class MockFactory : IMockFactory
    {
        Mock<ICompanyUnitOfWork> GetMockUnitOfWork()
        {
            var unitOfWork = new Mock<ICompanyUnitOfWork>();
            var companyRepository = new Mock<IRepository<Company>>();
            var employeRepository = new Mock<IRepository<Employe>>();
            unitOfWork.Setup(uow => uow.CompanyRepository).Returns(companyRepository.Object);
            unitOfWork.Setup(uow => uow.EmployeRepository).Returns(employeRepository.Object);
            
            return unitOfWork;
        }

        public ICompanyUnitOfWork CreateSimpleMockCompanyUnitOfWork()
        {
            return GetMockUnitOfWork().Object;
        }

        public ICompanyUnitOfWork CreateMockComapnyUnitOfWorkSaveAsyncThrowsException(Exception e)
        {
            var unitOfWork = GetMockUnitOfWork();
            unitOfWork.Setup(uow => uow.SaveAsync()).Throws(e);
            
            return unitOfWork.Object;
        }

        public ICompanyUnitOfWork CreateMockCompanyUnitOfWorkHasCompany()
        {
            var unitOfWork = new Mock<ICompanyUnitOfWork>();
            var companyRepository = new Mock<IRepository<Company>>();
            companyRepository.Setup(r => r.GetById(1)).ReturnsAsync(new Company()
                {
                    Name="test",
                    EstablishmentYear=2018
                });
            
            unitOfWork.Setup(uow => uow.CompanyRepository).Returns(companyRepository.Object);

            return unitOfWork.Object;
        }
    }
}