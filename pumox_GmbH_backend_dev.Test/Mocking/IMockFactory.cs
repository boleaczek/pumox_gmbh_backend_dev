using System;
using pumox_GmbH_backend_dev.UnitsOfWork;

namespace pumox_GmbH_backend_dev.Test.Mocking
{
    interface IMockFactory
    {
        ICompanyUnitOfWork CreateSimpleMockCompanyUnitOfWork();
        ICompanyUnitOfWork CreateMockComapnyUnitOfWorkSaveAsyncThrowsException(Exception e);
        ICompanyUnitOfWork CreateMockCompanyUnitOfWorkHasCompany();
    }
}