CREATE TABLE Companies(
    Id BIGINT IDENTITY(1,1),
    Name VARCHAR(20) NOT NULL,
    EstablishmentYear INT NOT NULL,
    PRIMARY KEY(Id)
);

CREATE TABLE Employes(
    Id BIGINT IDENTITY(1,1),
    Name VARCHAR(20) NOT NULL,
    LastName VARCHAR(20) NOT NULL,
    DateOfBirth DATE,
    CompanyId BIGINT,
    JobTitle INT NOT NULL,
    PRIMARY KEY(Id),
    CONSTRAINT FK_EmployeCompany FOREIGN KEY(CompanyId)
    REFERENCES Companies(Id) ON DELETE CASCADE
);
