# Company management API
## Compilation and running
### How to compile
    1. Go to pumox_GmbG_backend_dev directory.
    2. Run `dotnet build`.
    3. Run:
        3.1 `dotnet publish -c Release -r win10-x64` for Windows.
        3.2 `dotnet publish -c Release -r linux-x64` for Linux.
### How to run
    1. Go to /pumox_GmbH_backend_dev/bin/Release/netcoreapp2.1/win10-x64/publish.
    2. Set "DefaultConnection" property in appsettings.json to to desired database.
    3.. Run pumox_GmbH_backend_dev, use `--server.urls "https://localhost:<port number>"` to run on a non default port.

## Authorization
Username: UID
Password: TOODXA